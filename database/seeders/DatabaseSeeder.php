<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    // ============ User ============
        User::factory(3)->create();
        // \App\Models\User::factory(10)->create();
        // User::create([
        //     'name' => 'Sandhika Galih',
        //     'email' => 'sandhikagalih@gmail.com',
        //     'password' => bcrypt('12345')
        // ]);
        // User::create([
        //     'name' => 'Doddy Ferdiansyah',
        //     'email' => 'doddy@gmail.com',
        //     'password' => bcrypt('12345')
        // ]);
    // ============ /.User ============

    // ============ Category ============
        Category::create([
            'name' => 'Web Programming',
            'slug' => 'web-programming'
        ]);

        Category::create([
            'name' => 'Web Design',
            'slug' => 'web-design'
        ]);

        Category::create([
            'name' => 'Personal',
            'slug' => 'personal'
        ]);
    // ============ /.Category ============

    // ============ Post ============
        Post::factory(20)->create();
        // Post::create([
        //     'title' => 'Judul Pertama',
        //     'slug' => 'judul-pertama',
        //     'excerpt' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus ratione officia assumenda voluptas,',
        //     'body' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus ratione officia assumenda voluptas, saepe quaerat tempore est suscipit accusantium, dolor veritatis qui ipsa. Vel illo atque expedita voluptate obcaecati nemo laborum labore tenetur nostrum? Deserunt inventore similique cupiditate dicta suscipit error nostrum. Molestias consequatur beatae tenetur illo reprehenderit sint placeat enim ut velit doloribus reiciendis mollitia odit culpa at quis totam voluptas, dicta aperiam id delectus perspiciatis minus officiis provident cupiditate. Nam eligendi voluptatum, voluptate dolores magnam excepturi, nisi harum dolore dolorem est aliquam provident ducimus, officiis nostrum? Fugit repellendus similique quis maiores ad perspiciatis voluptates vel voluptas perferendis. Dolorum!',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);

        // Post::create([
        //     'title' => 'Judul Ke Dua',
        //     'slug' => 'judul-ke-dua',
        //     'excerpt' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus ratione officia assumenda voluptas,',
        //     'body' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus ratione officia assumenda voluptas, saepe quaerat tempore est suscipit accusantium, dolor veritatis qui ipsa. Vel illo atque expedita voluptate obcaecati nemo laborum labore tenetur nostrum? Deserunt inventore similique cupiditate dicta suscipit error nostrum. Molestias consequatur beatae tenetur illo reprehenderit sint placeat enim ut velit doloribus reiciendis mollitia odit culpa at quis totam voluptas, dicta aperiam id delectus perspiciatis minus officiis provident cupiditate. Nam eligendi voluptatum, voluptate dolores magnam excepturi, nisi harum dolore dolorem est aliquam provident ducimus, officiis nostrum? Fugit repellendus similique quis maiores ad perspiciatis voluptates vel voluptas perferendis. Dolorum!',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);

        // Post::create([
        //     'title' => 'Judul Ke Tiga',
        //     'slug' => 'judul-ke-tiga',
        //     'excerpt' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus ratione officia assumenda voluptas,',
        //     'body' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus ratione officia assumenda voluptas, saepe quaerat tempore est suscipit accusantium, dolor veritatis qui ipsa. Vel illo atque expedita voluptate obcaecati nemo laborum labore tenetur nostrum? Deserunt inventore similique cupiditate dicta suscipit error nostrum. Molestias consequatur beatae tenetur illo reprehenderit sint placeat enim ut velit doloribus reiciendis mollitia odit culpa at quis totam voluptas, dicta aperiam id delectus perspiciatis minus officiis provident cupiditate. Nam eligendi voluptatum, voluptate dolores magnam excepturi, nisi harum dolore dolorem est aliquam provident ducimus, officiis nostrum? Fugit repellendus similique quis maiores ad perspiciatis voluptates vel voluptas perferendis. Dolorum!',
        //     'category_id' => 2,
        //     'user_id' => 1
        // ]);
        
        // Post::create([
        //     'title' => 'Judul Ke Empat',
        //     'slug' => 'judul-ke-empat',
        //     'excerpt' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus ratione officia assumenda voluptas,',
        //     'body' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus ratione officia assumenda voluptas, saepe quaerat tempore est suscipit accusantium, dolor veritatis qui ipsa. Vel illo atque expedita voluptate obcaecati nemo laborum labore tenetur nostrum? Deserunt inventore similique cupiditate dicta suscipit error nostrum. Molestias consequatur beatae tenetur illo reprehenderit sint placeat enim ut velit doloribus reiciendis mollitia odit culpa at quis totam voluptas, dicta aperiam id delectus perspiciatis minus officiis provident cupiditate. Nam eligendi voluptatum, voluptate dolores magnam excepturi, nisi harum dolore dolorem est aliquam provident ducimus, officiis nostrum? Fugit repellendus similique quis maiores ad perspiciatis voluptates vel voluptas perferendis. Dolorum!',
        //     'category_id' => 2,
        //     'user_id' => 2
        // ]);
    // ============ /.Post ============

    }
}
