<?php

namespace App\Models;

class Post
{
    private static $blog_posts = [
        [
            "title" => "Judul Post Pertama",
            "slug" => "judul-post-pertama",
            "author" => "Sandhika Galih",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta, voluptate animi veritatis saepe voluptatibus libero maiores accusantium dignissimos. Mollitia, iusto ut. Temporibus qui fugit illum nobis. Neque sed optio sequi?"
        ],
        [
            "title" => "Judul Post Kedua",
            "slug" => "judul-post-kedua",
            "author" => "Doddy Ferdiansyah",
            "body" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quae porro, eligendi excepturi accusamus itaque beatae molestiae facere libero fuga quos voluptatem totam recusandae ex est eaque natus possimus, sapiente numquam enim at id necessitatibus! Repellat fugit temporibus vel sunt totam dolore! Ipsam id adipisci quis deleniti aperiam nam placeat, deserunt culpa. Nihil vero laboriosam temporibus impedit fuga incidunt esse, officiis quaerat molestiae eos quibusdam tempore, aperiam corporis fugit vitae ipsam quam commodi quos porro accusantium itaque est unde animi iure! Consequatur cumque culpa sint excepturi beatae magnam, aspernatur libero aliquid, debitis ratione modi? Voluptate qui hic provident repellendus perferendis facilis?"
        ]
    ];

    public static function all()
    {
        return collect(self::$blog_posts);
    }

    public static function find($slug)
    {
        $posts = static::all();

        // $post=[];
        // foreach ($posts as $p) {
        //     if($p["slug"] === $slug) {
        //         $post = $p;
        //     }
        // }
        return $posts->firstWhere('slug', $slug);
    }
}
// 20:30