<?php

use App\Http\Controllers\CategoryController;
use App\Models\Post;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

use App\Models\Category;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home', [
        "title" => "Home",
        "active" => 'home'
    ]);
});
Route::get('/about', function () {
    return view('about', [
        "title" => "About",
        "active" => 'about',
        "name" => "Ivan Kristyanto",
        "email" => "MazterSamzz@gmail.com",
        "image" => "ivan.png"
    ]);
});

Route::get('/posts',[PostController::class, 'index']);
Route::get('posts/{post:slug}',[PostController::class, 'show']); // halaman single post

Route::get('/categories', [CategoryController::class, 'index']); // List Categories
Route::get('/categories/{category:slug}', [CategoryController::class, 'show']); // Filter by Category

Route::get('/authors/{author:username}', function (User $author) {
    return view('posts', [
        'title' => "Post by Author : $author->name",
        'posts' => $author->posts->load('category', 'author'),
    ]);
});